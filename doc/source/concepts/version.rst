Versioning
----------

Parts adds the notion of a formal version object.
This object allow for better handling of versions over raw strings 
for dealing with:

* Comparing and sorting of different versions
* Best match and range testing* 

See the Version Object for more details.
