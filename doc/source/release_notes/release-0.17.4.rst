***************
Release  0.17.4
***************

* Update a verbosemsgf to handle case of no arguments better that cause a crash
* some internal clean up testing for keys vs exception handling on KeyError
