***************
Release  0.18.5
***************
* Fix regression in tool loading with the `--toolchain` options when given a best version mapping.
* Tweak to version check logic for general GNU based tools.
