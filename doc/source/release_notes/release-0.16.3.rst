***************
Release  0.16.3
***************

* Add missing PACKAGE_SYSTEM_BIN variable
* Add back an ExportItem call internally that was needed
* Fix some issue with git update and checkout logic with extern items
* add more verbose messages for loading stats
* Fix rare crash in get_timestamp for directory nodes
* clean up in AutoMake builder to improve default load times
* address coloring codes in all.log
