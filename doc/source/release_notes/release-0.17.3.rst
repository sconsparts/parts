***************
Release  0.17.3
***************

* Update a verbose message for better information reporting for toolchains
* update base SCons to 4.4.0
* update an import to Jobs object to use new location in SCons
* Add hot patch to Clone bug until a SCons 4.5.1 or newer drop is made
